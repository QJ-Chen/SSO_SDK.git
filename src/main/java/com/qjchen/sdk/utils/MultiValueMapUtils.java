package com.qjchen.sdk.utils;

import com.qjchen.common.exception.ForbiddenException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class MultiValueMapUtils<T> {

    public static <T> MultiValueMap<String,String> fusion(T t){
        MultiValueMap<String,String> multiValueMap = new LinkedMultiValueMap<>();
        Field[] fields = t.getClass().getDeclaredFields();
        Arrays.stream(fields).forEach(field -> {
            field.setAccessible(true);
            try {
                String key = field.getName();
                Object value = field.get(t);
                if (value != null){
                    if (value instanceof List){
                        List<String> values = (List<String>) value;
                        values.stream().forEach(val -> {
                            multiValueMap.add(key,val);
                        });
                    }else{
                        multiValueMap.add(key,value.toString());
                    }
                }
            } catch (IllegalAccessException e) {
                throw new ForbiddenException("");
            }
        });
        return multiValueMap;
    }
}
