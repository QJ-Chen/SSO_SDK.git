package com.qjchen.sdk.config;

import com.qjchen.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SDKPropertiesConfig {

    private static final Logger log = LoggerFactory.getLogger(SDKPropertiesConfig.class);

    public static String SERVER_CONTEXT;

    public static String CLIENT_CONTEXT;

    public static String APP_CODE;

    public static String SSO_API;

    @Autowired
    private Environment environment;

    @Bean
    public void setPropertiesConfig(){
        log.info("====================> SDK属性开始配置 <====================");

        String serverContext = environment.getProperty("qjc.sso.server-context");
        if (StringUtils.isEmpty(serverContext)){
            log.error("服务端地址<qjc.sso.server-context>没有配置，请配置后再启动程序");
        }
        SERVER_CONTEXT = serverContext;
        log.info("完成服务端地址「{}」的配置注入",SERVER_CONTEXT);

        String clientContext = environment.getProperty("qjc.sso.client-context");
        if (StringUtils.isEmpty(clientContext)){
            log.error("客户端地址<qjc.sso.client-context>没有配置，请配置后再启动程序");
        }
        CLIENT_CONTEXT = clientContext;
        log.info("完成客户端地址「{}」的配置注入",CLIENT_CONTEXT);

        String appCode = environment.getProperty("qjc.sso.client-app-code");
        if (StringUtils.isEmpty(appCode)){
            log.warn("***************************************************************************************");
            log.error("应用编码为空，这会影响到系统的正常运行，请检查确认无误后再运行系统。建议配置<qjc.sso.client-app-code>后再启动");
            log.warn("***************************************************************************************");
            throw new RuntimeException("应用编码为空，这会影响到系统的正常运行，请检查确认无误后再运行系统。请配置<qjc.sso.client-app-code>后再启动");
        }
        APP_CODE = appCode;
        log.info("完成应用编码「{}」添加",appCode);

        String ssoApi = environment.getProperty("qjc.sso.server-api");
        if (StringUtils.isEmpty(appCode)){
            log.warn("***************************************************************************************");
            log.error("服务器接口为空，这可能会影响到系统的正常运行。建议配置<qjc.sso.api>后再启动");
            log.warn("***************************************************************************************");
        }
        SSO_API = ssoApi;
        log.info("完成接口「{}」添加",ssoApi);

        log.info("====================> SDK属性完成配置 <====================");
    }
}
