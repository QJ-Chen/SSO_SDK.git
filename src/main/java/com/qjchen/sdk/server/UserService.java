package com.qjchen.sdk.server;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.config.PropertiesConfig;
import com.qjchen.common.exception.BusinessException;
import com.qjchen.common.exception.ForbiddenException;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.vo.UserVO;
import com.qjchen.common.utils.CookieUtil;
import com.qjchen.sdk.config.SDKPropertiesConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class UserService extends BusinessSDK<UserVO,UserDTO>{


    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    /**
     * 使用token获取用户登录信息
     * @param request
     * @return
     */
    public UserVO getByToken(HttpServletRequest request) {
        HttpResponse executed = HttpRequest.post(SDKPropertiesConfig.SERVER_CONTEXT + SDKPropertiesConfig.SSO_API + "/user/getByToken")
                .header("id", CookieUtil.getCookie(request, "ID"))
                .header("token", CookieUtil.getCookie(request, PropertiesConfig.TOKEN_NAME))
                .header("appToken", PropertiesConfig.APP_TOKEN)
                .header("appCode", SDKPropertiesConfig.APP_CODE)
                .execute();
        Result result = JSONObject.parseObject(executed.body(), Result.class);
        if (result.getCode()!= 200){
            throw new ForbiddenException(result.getMessage());
        }
        return JSONObject.parseObject(JSONObject.toJSONString(result.getData()), UserVO.class);
    }

    public Result logOut(HttpServletRequest request){
        try {
            HttpResponse httpResponse = HttpRequest.get(SDKPropertiesConfig.SERVER_CONTEXT + SDKPropertiesConfig.SSO_API + "/user/logOut")
                    .header("appToken",PropertiesConfig.APP_TOKEN)
                    .header("appCode",SDKPropertiesConfig.APP_CODE)
                    .header("id", CookieUtil.getCookie(request, "ID"))
                    .header("token", CookieUtil.getCookie(request, PropertiesConfig.TOKEN_NAME))
                    .execute();
            Result result = JSONObject.parseObject(httpResponse.body(), Result.class);
            if (result.getCode() == 200){
                HttpSession session = request.getSession();
                log.debug("[ SSO-统一退出 ] ....start.... sessionId:{}", session.getId());
                //注销全局会话, SSOSessionListener 监听器会处理后续操作
                request.getSession().invalidate();
                log.debug("[ SSO-统一退出 ] ....end.... sessionId:{}", session.getId());
                return result;
            }
        }catch (Exception e){
            log.error("",e);
            throw new BusinessException("系统内部错误");
        }
        return null;
    }

    public UserVO get(HttpServletRequest request,UserDTO userDTO){
        Object select = super.select(request, userDTO, "/user/get");
        return JSONObject.parseObject(JSONObject.toJSONString(select), UserVO.class);
    }

    public List<UserVO> list(HttpServletRequest request,UserDTO userDTO){
        return (List<UserVO>) super.select(request,userDTO,"/user/list");
    }

    public Page<UserVO> page(HttpServletRequest request,UserDTO userDTO){
        return super.listPage(request,userDTO,"/user/listPage");
    }

}
