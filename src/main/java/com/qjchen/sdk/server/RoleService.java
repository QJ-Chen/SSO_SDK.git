package com.qjchen.sdk.server;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.vo.AuthVO;
import com.qjchen.common.model.vo.RoleVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class RoleService extends BusinessSDK<RoleVO, RoleDTO> {

    public RoleVO get(HttpServletRequest request,RoleDTO roleDTO) {
        Object select = super.select(request, roleDTO, "role/get");
        return JSONObject.parseObject(JSONObject.toJSONString(select), RoleVO.class);
    }

    public List<RoleVO> list(HttpServletRequest request,RoleDTO roleDTO) {
        Object select = super.select(request, roleDTO, "role/list");
        return JSONObject.parseObject(JSONObject.toJSONString(select),List.class);
    }

    public Page<RoleVO> page(HttpServletRequest request,RoleDTO roleDTO) {
        Page<RoleVO> roleVOPage = super.listPage(request, roleDTO, "role/listPage");
        return JSONObject.parseObject(JSONObject.toJSONString(roleVOPage),Page.class);

    }
}
