package com.qjchen.sdk.server;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.config.PropertiesConfig;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.vo.AppVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AppService extends BusinessSDK<AppVO,AppDTO>{

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    public Result<String> register(String serverContext,String api,AppDTO appDTO){
        try {
            HttpResponse httpResponse = HttpRequest.post(serverContext+api + "/app/register")
                    .body(JSONObject.toJSONString(appDTO))
                    .header("Content-Type", "application/json")
                    .execute();
            Result<String> result = JSONObject.parseObject(httpResponse.body(), Result.class);
            if (result!=null)
                if (result.getCode() == 200){
                    PropertiesConfig.APP_TOKEN = result.getData();
                }
                return result;
        }catch (RuntimeException e){
            log.error("认证中心连接建立失败，请稍后重试。",e);
        }
        throw new RuntimeException("认证中心异常无法启动应用，请稍后重试。");
    }

    public AppVO get(HttpServletRequest request,AppDTO appDTO){
        Object select = super.select(request, appDTO, "/app/get");
        return JSONObject.parseObject(JSONObject.toJSONString(select),AppVO.class);
    }

    public List<AppVO> list(HttpServletRequest request,AppDTO appDTO){
        Object select = super.select(request, appDTO, "/app/list");
        return JSONObject.parseObject(JSONObject.toJSONString(select),List.class);
    }

    public Page<AppVO> page(HttpServletRequest request,AppDTO appDTO){
        Page<AppVO> appVOPage = super.listPage(request, appDTO, "/app/listPage");
        return JSONObject.parseObject(JSONObject.toJSONString(appVOPage),Page.class);
    }
}
