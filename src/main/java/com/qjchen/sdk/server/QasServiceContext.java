package com.qjchen.sdk.server;

public class QasServiceContext {

    public static UserService getUserService = new UserService();

    public static AppService getAppService = new AppService();

    public static PlateService getPlateService = new PlateService();

    public static RoleService getRoleService = new RoleService();

    public static AuthService getAuthService = new AuthService();

}
