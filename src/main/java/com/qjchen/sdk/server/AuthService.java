package com.qjchen.sdk.server;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.AuthDTO;
import com.qjchen.common.model.dto.RoleDTO;
import com.qjchen.common.model.vo.AuthVO;
import com.qjchen.common.model.vo.RoleVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AuthService extends BusinessSDK<AuthVO, AuthDTO> {

    public AuthVO get(HttpServletRequest request, AuthDTO authDTO) {
        Object select = super.select(request, authDTO, "/auth/get");
        return JSONObject.parseObject(JSONObject.toJSONString(select), AuthVO.class);
    }

    public List<AuthVO> list(HttpServletRequest request, AuthDTO authDTO) {
        Object select = super.select(request, authDTO, "auth/list");
        return JSONObject.parseObject(JSONObject.toJSONString(select),List.class);
    }

    public Page<AuthVO> page(HttpServletRequest request, AuthDTO authDTO) {
        Page<AuthVO> authVOPage = super.listPage(request, authDTO, "auth/listPage");
        return JSONObject.parseObject(JSONObject.toJSONString(authVOPage),Page.class);
    }
}
