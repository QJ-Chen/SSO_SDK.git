package com.qjchen.sdk.server;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.config.PropertiesConfig;
import com.qjchen.common.exception.ForbiddenException;
import com.qjchen.common.interceptor.Result;
import com.qjchen.common.model.Page;
import com.qjchen.common.utils.ConvertUtil;
import com.qjchen.common.utils.CookieUtil;
import com.qjchen.sdk.config.SDKPropertiesConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class BusinessSDK<R,T> {

    private static final Logger log = LoggerFactory.getLogger(BusinessSDK.class);

    public Object select(HttpServletRequest request,T t, String api){
        try {
            HttpResponse response = HttpRequest.post(SDKPropertiesConfig.SERVER_CONTEXT + SDKPropertiesConfig.SSO_API + api)
                    .body(JSONObject.toJSONString(t))
                    .header("Content-Type", "application/json")
                    .header("id", CookieUtil.getCookie(request, "ID"))
                    .header("token", CookieUtil.getCookie(request, PropertiesConfig.TOKEN_NAME))
                    .header("appToken", PropertiesConfig.APP_TOKEN)
                    .header("appCode", SDKPropertiesConfig.APP_CODE)
                    .execute();
            Result result = JSONObject.parseObject(response.body(), Result.class);
            if (result.getCode() == 200){
                return result.getData();
            }
            throw new ForbiddenException(result.getMessage());
        }catch (RuntimeException e){
            log.error("认证中心连接建立失败，请稍后重试。",e);
            throw new RuntimeException("SDK调用异常，请稍后重试。");
        }
    }

    public Page<R> listPage(HttpServletRequest request,T t,String api){
        try {
            HttpResponse response = HttpRequest.post(SDKPropertiesConfig.SERVER_CONTEXT + SDKPropertiesConfig.SSO_API + api)
                    .body(JSONObject.toJSONString(t))
                    .header("Content-Type", "application/json")
                    .header("id", CookieUtil.getCookie(request, "ID"))
                    .header("token", CookieUtil.getCookie(request, PropertiesConfig.TOKEN_NAME))
                    .header("appToken", PropertiesConfig.APP_TOKEN)
                    .header("appCode", SDKPropertiesConfig.APP_CODE)
                    .execute();
            Result result = JSONObject.parseObject(response.body(), Result.class);
            if (result.getCode() == 200){
                return JSONObject.parseObject(JSONObject.toJSONString(result.getData()), Page.class);
            }
            throw new ForbiddenException(result.getMessage());
        }catch (RuntimeException e){
            log.error("认证中心连接建立失败，请稍后重试。",e);
            throw new RuntimeException("SDK调用异常，请稍后重试。");
        }
    }
}
