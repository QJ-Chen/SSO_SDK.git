package com.qjchen.sdk.server;

import com.alibaba.fastjson.JSONObject;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.AuthDTO;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.vo.AuthVO;
import com.qjchen.common.model.vo.PlateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class PlateService extends BusinessSDK<PlateVO,PlateDTO>{

    private static final Logger log = LoggerFactory.getLogger(PlateService.class);

    public PlateVO get(HttpServletRequest request,PlateDTO plateDTO){
        Object select = super.select(request, plateDTO, "/plate/get");
        return JSONObject.parseObject(JSONObject.toJSONString(select), PlateVO.class);
    }

    public List<PlateVO> list(HttpServletRequest request, PlateDTO plateDTO) {
        Object select = super.select(request, plateDTO, "plate/list");
        return JSONObject.parseObject(JSONObject.toJSONString(select),List.class);
    }

    public Page<PlateVO> page(HttpServletRequest request, PlateDTO plateDTO) {
        Page<PlateVO> authVOPage = super.listPage(request, plateDTO, "plate/listPage");
        return JSONObject.parseObject(JSONObject.toJSONString(authVOPage),Page.class);
    }
}
